import Vue from 'vue'
import VueRouter from 'vue-router'

import Store from '@/store'

import Util from '@/misc/utilities.js'

// sync routes
import Logout from './views/Logout.vue'
import Login from './views/Login.vue'
import Home from '@/views/Home.vue'
import PageNotFound from './views/PageNotFound.vue'
import PurchasePackage from './views/PurchasePackage.vue'
import PurchasePackageSuccess from './views/PurchasePackageSuccess.vue'

// async routes
  const Packages = () => import('./views/Packages.vue')

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'is-active-trail',
  linkExactActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/packages',
      component: Home,
      meta: {
        auth: true,
        title: Util.appTitle({
          title: 'Home'
        }),
      },
    },

    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: Util.appTitle({
          title: 'Login'
        }),
        redirectAuthenticated: 'package-list',
      },
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
      meta: {
        title: Util.appTitle({
          title: 'Logout'
        }),
      },
    },

    {
      path: '/packages',
      name: 'packages',
      component: Packages,
      meta: {
        auth: true,
        title: Util.appTitle({
          title: 'Packages'
        }),
      },
    },
    {
      path: '/packages/:id/purchase',
      name: 'purchasePackage',
      component: PurchasePackage,
      meta: {
        auth: true,
        title: Util.appTitle({
          title: 'Purchase package'
        }),
      },
    },
    {
      path: '/packages/:id/purchase/success',
      name: 'purchasePackageSuccess',
      component: PurchasePackageSuccess,
      meta: {
        auth: true,
        title: Util.appTitle({
          title: 'Purchase package success'
        }),
      },
    },

    {
      path: '/404',
      name: '404',
      component: PageNotFound,
      meta: {
        title: Util.appTitle({
          title: '404 Page not found'
        }),
      },
    },
    {
      path: '*',
      name: '*',
      redirect: '/404',
    },
  ]
})

router.beforeEach((to, current, next) => {
  const elApp = document.querySelector('#app')
  let target = null

  // head & meta tags
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Turn the meta tag definitions into actual elements in the head.
    if (nearestWithMeta) {
      nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
          tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
      })
      // Add the meta tags to the document head.
      .forEach(tag => document.head.appendChild(tag));
    }

  // authenticated route validation
  if (Util.isNull(target) && to.meta.auth && !Store.state.User.token) {
    target = '/login'
  }

  // redirect to defined route for authenticated user
  if(Util.isNull(target) && Store.state.User.token && to.meta.redirectAuthenticated && to.meta.redirectAuthenticated !== to.name) {
    target = {name: to.meta.redirectAuthenticated}
  }

  Util.scrollToElm({
    container: elApp,
    elm: elApp
  })

  if (target == null) {
    next()
  }
  else {
    next(target);
  }
})

export default router
