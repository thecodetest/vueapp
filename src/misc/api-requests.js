import Vue from 'vue'


// const login = (credentials) => {
const login = () => {
  return Vue.prototype.$axios({
    method: 'get',
    url: '/login-member.json',
    // auth: credentials,
  })
}


const accountInfo = () => {
  return Vue.prototype.$axios({
    method: 'get',
    url: '/account-info-member.json',
  })
}


const packageList = () => {
  return Vue.prototype.$axios({
    method: 'get',
    url: '/package-list.json',
  }).catch( (err) => {
    if (err.response) {
      Vue.prototype.$toasted.show('Packages not found', {
        type: 'error',
        position: 'top-center'
      })
    }
    else {
      Vue.prototype.$toasted.show('Connection error', {
        type: 'error',
        position: 'bottom-center'
      })
    }
  })
}


export default {
  login: login,

  accountInfo: accountInfo,

  packageList: packageList,
}