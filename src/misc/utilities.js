import isEmpty from 'lodash.isempty'

const appName = "App"
const appTitleSuffix = ' - ' + appName


const scrollToElm = (param={}) => {
  let {container, elm, duration=500, onDone} = param

  duration = duration / 1000

  const easeInOutQuad = (t) => {
    return t<.5 ? 2*t*t : -1+(4-2*t)*t
  };

  let to = elm.offsetTop
  let start = container.scrollTop
  let change = to - start
  let startTime = performance.now()
  let now, elapsed, t

  const animateScroll = () => {
    now = performance.now();
    elapsed = (now - startTime)/1000;
    t = (elapsed/duration);

    container.scrollTop = start + change * easeInOutQuad(t);

    if( t < 1 )
      window.requestAnimationFrame(animateScroll);
    else
      onDone && onDone();
  };

  animateScroll();
}


const appTitle = ({title, suffix = appTitleSuffix}) => {
  return title + suffix
}


const pagerCount = (totalItems, limit) => {
  return Math.ceil(totalItems / limit)
}


const isNull = (value) => {
  return value === null
}


const isValid = (value) => {
  return !isNull(value) && value !== undefined
}


const percentOf = (percent, total) => {
  return ( percent / 100 ) * total
}


export default {
  appName: appName,
  appTitleSuffix: appTitleSuffix,
  appTitle: appTitle,
  pagerCount: pagerCount,
  isNull: isNull,
  isValid: isValid,
  isEmpty: isEmpty,
  percentOf: percentOf,
  scrollToElm: scrollToElm,
}