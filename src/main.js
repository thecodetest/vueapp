import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'

import Axios from 'axios'
import Toasted from 'vue-toasted';
import VueElementLoading from 'vue-element-loading'
import localStorage from 'vue-ls';
import { default as VeeValidate } from 'vee-validate'

import Api from '@/misc/api-requests.js'
import Util from '@/misc/utilities.js'

import '@/assets/css/minireset.min.css'
import '@/assets/css/fonts.css'
import '@/assets/css/defaults.css'
import '@/assets/css/layout.css'
import '@/assets/css/forms.css'

Vue.prototype.$api = Api
Vue.prototype.$util = Util
Vue.prototype.$axios = Axios

Vue.use(localStorage, {
  namespace: 'app__',
  name: 'appStorage',
  storage: 'local',
})

Vue.use(Toasted, {
  theme: 'toasted-primary',
  duration: 3000,
})

Vue.use(VeeValidate)

Vue.component('VueElementLoading', {
  extends: VueElementLoading,
  props: {
    color: {
      type: String,
      default: '#3F3A5E',
    },
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
