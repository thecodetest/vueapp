export default {
  namespaced: true,
  state: {
    current: null,
    promocode: null,
  },
  mutations: {
    SET_CURRENT: function(state, value) {
      state.current = value;
    },
    SET_PROMOCODE: function(state, value) {
      state.promocode = value;
    },
  },
  actions: {
    set_current: (context, value) => {
      context.commit('SET_CURRENT', value);
    },
    set_promocode: (context, value) => {
      context.commit('SET_PROMOCODE', value);
    },
  },
}